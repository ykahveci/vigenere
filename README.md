Vigenére
========

## About

What is this?

* A simple python script that allows encryption and decryption with the Vigenére algorithm
* Both the terminal interface and the spaghetti code should be rather self-explanatory

## Installation

1. Install Python on your computer
2. Clone the repository with `git clone git@gitlab.com:ykahveci/vigenere.git`
3. Run the script

## Usage

### Features

* A terminal menu interface
* Encryption with plaintext and key
* Decryption with ciphertext and key

## Contributing / Reporting issues

If you have an idea on how to improve this script, feel free to [open a merge request](https://gitlab.com/ykahveci/vigenere/-/merge_requests/new).

If you have any problems with this script, please [check](https://gitlab.com/ykahveci/vigenere/-/issues) if someone else has already had the problem and if not, [open a new issue](https://gitlab.com/ykahveci/vigenere/-/issues/new).

## License

This project is licensed under the MIT License. For details, please refer to the LICENSE file.
