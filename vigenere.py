##############################
# Welcome to spaghetti code! #
##############################

from string import ascii_uppercase as uppercase_characters

###################################
# Find the index of the character #
###################################

def findCharIndex(char):
    letter_key = 0
    for letter_value in uppercase_characters:
        if letter_value == char:
            return(letter_key)
        letter_key = letter_key + 1

#############################################
# Generate a dictionary for en-/ decryption #
#############################################

def generateDictionary(key_char_index):
    return str.maketrans(uppercase_characters, uppercase_characters[key_char_index:] + uppercase_characters[:key_char_index])

###################################################################
# En-/ Decryption                                                 #
# (when decrypting, swap plaintext and ciphertext in the comments #
###################################################################

def crypt(plaintext, key, decrypt: bool = False):
    plaintext = plaintext.upper()
    key = key.upper()
    ciphertext = ""
    counter = 0

    for character in plaintext: # Iterates over all characters
        pass_char_test = False # If the current character is not a latin letter, append it to the ciphertext and continue with the next character.
        for letter_value in uppercase_characters:
            if letter_value == character:
                pass_char_test = True
        if pass_char_test == False:
            ciphertext += character
            continue

        if decrypt: # Generates the en-/ decryption dictionary
            current_conversion_dictionary = generateDictionary(-findCharIndex(key[counter]))
        else:
            current_conversion_dictionary = generateDictionary(findCharIndex(key[counter]))

        ciphertext += character.translate(current_conversion_dictionary) # En-/ decrypts the character using the dictionary
        counter += 1 # Increases the counter of the current character of the key
        if counter == len(key): # Once the end of the key has been reached, reset the counter to 0 (=first char of key)
            counter = 0

    return(ciphertext) # Returns the ciphertext

########
# Menu #
########

while True:
    choice = input("\nType [E] to encrypt plaintext and [D] to decrypt ciphertext: ")

    if (choice == "e" or choice == "E"):
        ciphertext = crypt(input("\nPlease Enter the Plaintext: "), input("Please Enter the Key: "), False)
        print("\nCiphertext:", ciphertext)
        break

    elif (choice == "d" or choice == "D"):
        plaintext = crypt(input("\nPlease Enter the Ciphertext: "), input("Please Enter the Key: "), True)
        print("\nPlaintext:", plaintext)
        break

    else:
        print("\nYour goals are beyond my understanding.")
        pass
